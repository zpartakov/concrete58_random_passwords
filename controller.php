<?php
/**
 * The controller for the randomPasswords package.
 *
 * @package    randomPasswords
 *
 * @author     Fred Radeff <fradeff@akademia.ch>
 * @copyright  Copyright (c) 2018 radeff.red (https://radeff.red)
 * @license    http://www.gnu.org/licenses/ GNU General Public License
 */

namespace Concrete\Package\Concrete58RandomPasswords;
use Package;
use BlockType;
use View;
use Loader;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {

	protected $pkgHandle = 'concrete58_random_passwords';
	protected $appVersionRequired = '5.8.4.3';
	protected $pkgVersion = '1.0.1';


	public function getPackageName()
	{
		return t("Concrete58RandomPasswords");
	}

	public function getPackageDescription()
	{
		return t("Generate Random Passwords");
	}

	public function install()
	{
		$pkg = parent::install();

		// install block
		BlockType::installBlockTypeFromPackage('randompasswords', $pkg);

		return $pkg;
	}
	public function uninstall() {
		parent::uninstall();
	}

}
