<?php  defined('C5_EXECUTE') or die(_("Access Denied."));

//echo phpinfo();
function passe_mnemo(){
	#Description :
	#Génère un mot de passe prononçable, pour faciliter sa mémorisation, mais malgré tout très compliqué.
	#Par exemple :
	#ZbleUrg (prononçable, mais difficile).
	#Auteur : Damien Seguy
	#Url : http://www.nexen.net
	if (func_num_args() == 1){ $nb = func_get_arg(0);} else { $nb = 6;}

	// on utilise certains chiffres : 1 = i, 5 = S, 6=b, 3=E, 9=G, 0=O
	$lettre = array();
	/*
	 * mod radeff: suppressed i, I, 1, o, O and 0; added symbols !,.
	*
	*/
$lettre[0] = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'j', 'k', 'm', 'n', 'p', 'q', 'r',
			's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A',
			'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
			'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'D',
			'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '9', '6', '5', '3','!',',','.');
	$lettre[1] = array('a', 'e', 'o', 'u', 'y', 'A', 'E',
			'I',  'U', 'Y' , '3' );
	$lettre[-1] = array('b', 'c', 'd', 'f', 'g', 'h', 'j', 'k',
			'm', 'n', 'p', 'q', 'r', 's', 't',
			'v', 'w', 'x', 'z', 'B', 'C', 'D', 'F',
			'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P',
			'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Z',
			'5', '6', '9');

	$retour = "";
	$prec = 1;
	$precprec = -1;
	srand((double)microtime()*20001107);
	while(strlen($retour) < $nb){
		// pour genere la suite de lettre, on dit : si les deux lettres sonts
		// des consonnes (resp. des voyelles) on affiche des voyelles (resp, des consonnes).
		// si les lettres sont de type differents, on affiche une lettre de l'alphabet
		$type = ($precprec + $prec)/2;
		$r = $lettre[$type][array_rand($lettre[$type], 1)];
		$retour .= $r;
		$precprec = $prec;
		$prec = in_array($r, $lettre[-1]) - in_array($r, $lettre[1]);

	}
	echo $retour;

}

$nombredecolonnes=4;

?>
<style>
.multicol {
	 -webkit-columns: <?php echo $nombredecolonnes;?> auto;
	 -moz-columns: <?php echo $nombredecolonnes;?> auto;
	 columns: <?php echo $nombredecolonnes;?> auto;
		-moz-column-gap: 20px;
-webkit-column-gap: 20px;
column-gap: 20px;
}
</style>
<?php

echo "<h2>Générateur de mot de passe aléatoires</h2>";
if(!($_GET['longueur'])){
$longueur=12;
$nbpwd=20;
} else {
	$longueur=$_GET['longueur'];
	$nbpwd=$_GET['nbpwd'];
}
echo '
<form method="get">
<table class="table table-striped table-bordered table-hover">
<tr><td>Longueur des mots de passe</td><td><input type="text" name="longueur" value="'.$longueur .'" /></td></tr>
<tr><td>Nombre de mots de passe</td><td><input type="text" name="nbpwd"  value="'.$nbpwd .'" /></td></tr>
<tr><td><a class="btn btn-primary" href="'.$_SERVER['SCRIPT_URL'].'">Annuler</a></td><td><input type="submit" class="btn btn-success"></td></tr>
</table>
</form>
';

echo "<div class=\"multicol\">";
for($i=0;$i<$nbpwd;$i++) {
//echo passe_mnemo($longueur-2);
	passe_mnemo($longueur-2);
echo "<br>";
}
echo "</div>";
?>
