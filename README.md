# Concrete58RandomPasswords
[![Concrete58RandomPasswords](//radeff.red/pics/git/Concrete58RandomPasswords.png)](Concrete58RandomPasswords)

A block for Concrete5.8 displaying random passwords

## Français
un block concrete5 pour générer des mots de passe aléatoires.

# Instructions pour l'installation

- Installer le package sous votre répertoire /packages
- Chercher dans votre interface d'administration "Améliorer concrete5"
- Chercher le packages _"Concrete58RandomPasswords"_
- Installer le package

Vous pouvez maintenant inclure le bloc Randompasswords sur votre site

## English:
a block to generate random passwords

# Installation Instructions
- Install package in your site's /packages directory
- Go to "Extend concrete5 > Add Functionality"
- Activate package _"Concrete58RandomPasswords"_

You may now include the block anywhere on your concrete5 website
